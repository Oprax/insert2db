from pathlib import Path
from configparser import ConfigParser

from bottle import Bottle, run, request, response

from json2sql import json2sql
from orcl_wrapper import OrclWrapper
from cx_Oracle import DatabaseError


ROOT = Path(__file__).resolve().parent

cfg = ConfigParser()
cfg.read(str(ROOT / 'conf.ini'))

oracle = OrclWrapper(
    user=cfg['oracle'].get('user'),
    pswd=cfg['oracle'].get('password'),
    connection=cfg['oracle'].get('connection')
)

app = Bottle()


@app.route('/')
def index():
    return {
        "endpoint": {
            "path": "/insert",
            "method": "post"
        },

    }


@app.post('/insert')
def insert():
    gen = json2sql(request.json, cfg['oracle'].get('db_name'))
    print(gen._json)
    try:
        sqls = gen.get_sql_inserting()
        tmp_sql = ''
        for sql in sqls:
            oracle.exec(sql)
            tmp_sql = sql
        oracle.commit()
    except DatabaseError as e:
        response.status = 400
        print(e, tmp_sql)
        return {
            "msg": f"{e}",
            "sql": sqls
        }
    return {
        "msg": ""
    }


def main():
    run(app,
        host=cfg['webservice'].get('host', '0.0.0.0'),
        port=cfg['webservice'].getint('port', 8080),
        reloader=cfg['webservice'].getboolean('debug', True),
        debug=cfg['webservice'].getboolean('debug', True))


if __name__ == '__main__':
    main()
