import json
import re


RE_DATETIME = re.compile(r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}")


class json2sql():
    def __init__(self, raw_json, dbname=''):
        self._dbname = dbname

        if isinstance(raw_json, str):
            raw_json = json.loads(raw_json)

        if isinstance(raw_json, dict):
            self._json = [raw_json]
        else:
            self._json = raw_json

    def _extract_struct(self, index, table_name):
        return self._json[index][table_name][0].keys()

    def _parse_value(self, value):
        if value is None:
            return 'NULL'
        if isinstance(value, bool):
            return repr(int(value))
        if isinstance(value, str) and RE_DATETIME.match(value):
            return f"TO_DATE('{value}', 'YYYY-MM-DD HH24:MI:SS')"
        return repr(value)

    def get_sql_inserting(self):
        sqls = []
        for i, tables in enumerate(self._json):
            for table_name, objects in tables.items():
                sqls.append(f'DELETE FROM "{self._dbname.upper()}"."{table_name.upper()}"')
                keys = [f'"{k.upper()}"' for k in self._extract_struct(i, table_name)]
                sql = (f'INSERT INTO "{self._dbname.upper()}"."{table_name.upper()}" '
                       f"({', '.join(keys)})\nVALUES ")

                for obj in objects:
                    values = [self._parse_value(v) for v in obj.values()]
                    sqls.append(sql + f"({', '.join(values)})")
        return sqls
