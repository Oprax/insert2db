import cx_Oracle as orcl


class OrclWrapper:
    def __init__(self, user='', pswd='', connection=''):
        self._conn = orcl.connect(user, pswd, connection)
        self._cursor = None

    def exec(self, sql):
        if self._cursor is None:
            self._cursor = self._conn.cursor()
        self._cursor.execute(sql)

    def commit(self):
        self._cursor.execute("COMMIT")
        self._cursor = None
