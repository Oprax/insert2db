from json2sql import json2sql


def main():
    raw_json = ''
    with open('./generated.json', 'r') as fjson:
        raw_json = fjson.read()
    gen = json2sql(raw_json)
    sqls = gen.get_sql_inserting()
    with open('./result.sql', 'w') as fsql:
        for sql in sqls:
            fsql.write(sql)


if __name__ == '__main__':
    main()
