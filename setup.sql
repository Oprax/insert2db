-- C:\app\Administrateur\product\11.2.0\dbhome_1\network\admin\tnsnames.ora
-- sqlplus SYS/ToorToor1@192.168.88.10/orcl AS SYSDBA
-- sqlplus SYS/ToorToor1@172.16.88.8/orcl AS SYSDBA

CREATE TABLESPACE CANDY_TAB DATAFILE 'C:\app\Administrateur\oradata\orcl\CANDY_TAB\candy_tab_x1' SIZE 500M EXTENT MANAGEMENT LOCAL;

CREATE USER candy_admin IDENTIFIED BY ToorToor1 DEFAULT TABLESPACE CANDY_TAB;
ALTER USER candy_admin quota UNLIMITED ON CANDY_TAB;
ALTER USER candy_admin TEMPORARY TABLESPACE TEMP;

GRANT CREATE SESSION TO candy_admin;
GRANT SELECT, INSERT, UPDATE, DELETE ON "CANDY_ADMIN"."CLIENTS" TO candy_admin;

/*==============================================================*/
/* Nom de SGBD :  ORACLE Version 11g                            */
/* Date de création :  02/07/2018 22:32:18                      */
/*==============================================================*/


alter table candy_admin.CANDIES
   drop constraint FK_CANDIES_ASSOCIATI_CANDIES_;

alter table candy_admin.CANDIES
   drop constraint FK_CANDIES_ASSOCIATI_VARIETIES;

alter table candy_admin.CANDIES
   drop constraint FK_CANDIES_ASSOCIATI_TEXTURES;

alter table candy_admin.CANDIES
   drop constraint FK_CANDIES_ASSOCIATI_COLORS;

alter table candy_admin.CANDIES_ORDERS
   drop constraint FK_CANDIES__CANDIES_O_ORDERS;

alter table candy_admin.CANDIES_ORDERS
   drop constraint FK_CANDIES__CANDIES_O_CANDIES;

alter table candy_admin.COMPOSITIONS
   drop constraint FK_COMPOSIT_COMPOSITI_CANDIES_;

alter table candy_admin.COMPOSITIONS
   drop constraint FK_COMPOSIT_COMPOSITI_COMPONEN;

alter table candy_admin.CONDITIONNINGS
   drop constraint FK_CONDITIO_ASSOCIATI_CONTAINE;

alter table candy_admin.CONTAINER_CAPACITIES
   drop constraint FK_CONTAINE_CONTAINER_TRANSPOR;

alter table candy_admin.CONTAINER_CAPACITIES
   drop constraint FK_CONTAINER_CONTAINE2;

alter table candy_admin.CONTAINER_CAPACITIES
   drop constraint FK_CONTAINER_CONTAINE1;

alter table candy_admin.COUNTRIES
   drop constraint FK_COUNTRIE_ASSOCIATI_TRANSPOR;

alter table candy_admin.MANUFACTURINGS
   drop constraint FK_MANUFACT_ASSOCIATI_VARIETIES;

alter table candy_admin.ORDERS
   drop constraint FK_ORDERS_ASSOCIATI_CLIENTS;

alter table candy_admin.ORDERS
   drop constraint FK_ORDERS_ASSOCIATI_COUNTRIE;

alter table candy_admin.PRICES
   drop constraint FK_PRICES_PRICES_CANDIES_;

alter table candy_admin.PRICES
   drop constraint FK_PRICES_PRICES2_CONTAINE;

drop table candy_admin.CANDIES cascade constraints;

drop table candy_admin.CANDIES_ORDERS cascade constraints;

drop table candy_admin.CANDIES_TYPES cascade constraints;

drop table candy_admin.CLIENTS cascade constraints;

drop table candy_admin.COLORS cascade constraints;

drop table candy_admin.COMPONENTS cascade constraints;

drop table candy_admin.COMPOSITIONS cascade constraints;

drop table candy_admin.CONDITIONNINGS cascade constraints;

drop table candy_admin.CONTAINERS cascade constraints;

drop table candy_admin.CONTAINER_CAPACITIES cascade constraints;

drop table candy_admin.COUNTRIES cascade constraints;

drop table candy_admin.MANUFACTURINGS cascade constraints;

drop table candy_admin.ORDERS cascade constraints;

drop table candy_admin.PRICES cascade constraints;

drop table candy_admin.TEXTURES cascade constraints;

drop table candy_admin.TRANSPORTS cascade constraints;

drop table candy_admin.VARIETIES cascade constraints;


/*==============================================================*/
/* Table : CANDIES                                              */
/*==============================================================*/
create table candy_admin.CANDIES
(
   ID_CANDIES             INTEGER              not null,
   ID_CANDIES_TYPES        INTEGER              not null,
   ID_VARIETIES           INTEGER              not null,
   ID_TEXTURES           INTEGER              not null,
   ID_COLORS             INTEGER              not null,
   constraint PK_CANDIES primary key (ID_CANDIES)
);

/*==============================================================*/
/* Table : CANDIES_ORDERS                                       */
/*==============================================================*/
create table candy_admin.CANDIES_ORDERS
(
   ID_CANDIES             INTEGER              not null,
   ID_ORDERS             INTEGER              not null,
   QUANTITY             INTEGER              not null,
   constraint PK_CANDIES_ORDERS primary key (ID_CANDIES, ID_ORDERS)
);

/*==============================================================*/
/* Table : CANDIES_TYPES                                        */
/*==============================================================*/
create table candy_admin.CANDIES_TYPES
(
   ID_CANDIES_TYPES     INTEGER              not null,
   NAME                 CHAR(255)            not null,
   MANUFACTURING_COSTS  FLOAT                not null,
   PACKAGING_COSTS      FLOAT                not null,
   SHIPPING_COSTS       FLOAT                not null,
   OVERHEADS            FLOAT                not null,
   constraint PK_CANDIES_TYPES primary key (ID_CANDIES_TYPES)
);

/*==============================================================*/
/* Table : CLIENTS                                              */
/*==============================================================*/
create table candy_admin.CLIENTS
(
   ID_CLIENTS            INTEGER              not null,
   FIRST_NAME           CHAR(255)            not null,
   LAST_NAME            CHAR(255)            not null,
   ADDRESS              CHAR(1500),
   CREATED_AT           DATE                 not null,
   constraint PK_CLIENTS primary key (ID_CLIENTS)
);

/*==============================================================*/
/* Table : COLORS                                               */
/*==============================================================*/
create table candy_admin.COLORS
(
   ID_COLORS             INTEGER              not null,
   NAME                 CHAR(255)            not null,
   constraint PK_COLORS primary key (ID_COLORS)
);

/*==============================================================*/
/* Table : COMPONENTS                                           */
/*==============================================================*/
create table candy_admin.COMPONENTS
(
   ID_COMPONENTS         INTEGER              not null,
   NAME                 CHAR(255)            not null,
   CONDITIONNING        INTEGER              not null,
   PALLET_WEIGHT        INTEGER              not null,
   constraint PK_COMPONENTS primary key (ID_COMPONENTS)
);

/*==============================================================*/
/* Table : COMPOSITIONS                                         */
/*==============================================================*/
create table candy_admin.COMPOSITIONS
(
   ID_COMPONENTS         INTEGER              not null,
   ID_CANDIES_TYPES        INTEGER              not null,
   QUANTITY             INTEGER              not null,
   constraint PK_COMPOSITIONS primary key (ID_COMPONENTS, ID_CANDIES_TYPES)
);

/*==============================================================*/
/* Table : CONDITIONNINGS                                       */
/*==============================================================*/
create table candy_admin.CONDITIONNINGS
(
   ID_CONDITIONNINGS     INTEGER              not null,
   ID_CONTAINERS         INTEGER              not null,
   NAME                 CHAR(255)            not null,
   CADENCE              FLOAT                not null,
   DELAY                FLOAT                not null,
   constraint PK_CONDITIONNINGS primary key (ID_CONDITIONNINGS)
);

/*==============================================================*/
/* Table : CONTAINERS                                           */
/*==============================================================*/
create table candy_admin.CONTAINERS
(
   ID_CONTAINERS         INTEGER              not null,
   NAME                 CHAR(255)            not null,
   constraint PK_CONTAINERS primary key (ID_CONTAINERS)
);

/*==============================================================*/
/* Table : CONTAINER_CAPACITIES                                 */
/*==============================================================*/
create table candy_admin.CONTAINER_CAPACITIES
(
   ID_CONTAINER_CAPACITIES INTEGER            not null,
   ID_CONTAINERS         INTEGER              not null,
   CON_ID_CONTAINERS     INTEGER              not null,
   ID_TRANSPORTS         INTEGER,
   NB_CONTAINER_1        INTEGER,
   NB_CONTAINER_2        INTEGER,
   constraint PK_CONTAINER_CAPACITIES primary key (ID_CONTAINER_CAPACITIES)
);

/*==============================================================*/
/* Table : COUNTRIES                                            */
/*==============================================================*/
create table candy_admin.COUNTRIES
(
   ID_COUNTRIES           INTEGER              not null,
   ID_TRANSPORTS         INTEGER              not null,
   NAME                 CHAR(255)            not null,
   constraint PK_COUNTRIES primary key (ID_COUNTRIES)
);

/*==============================================================*/
/* Table : MANUFACTURINGS                                       */
/*==============================================================*/
create table candy_admin.MANUFACTURINGS
(
   ID_MANUFACTURINGS     INTEGER              not null,
   ID_VARIETIES           INTEGER              not null,
   NAME                 CHAR(255)            not null,
   CADENCE              FLOAT                not null,
   DELAY                FLOAT                not null,
   constraint PK_MANUFACTURINGS primary key (ID_MANUFACTURINGS)
);

/*==============================================================*/
/* Table : ORDERS                                               */
/*==============================================================*/
create table candy_admin.ORDERS
(
   ID_ORDERS             INTEGER              not null,
   ID_CLIENTS            INTEGER              not null,
   ID_COUNTRIES           INTEGER              not null,
   CREATED_AT           DATE                 not null,
   constraint PK_ORDERS primary key (ID_ORDERS)
);

/*==============================================================*/
/* Table : PRICES                                               */
/*==============================================================*/
create table candy_admin.PRICES
(
   ID_CONTAINERS         INTEGER              not null,
   ID_CANDIES_TYPES        INTEGER              not null,
   PRICE                FLOAT                not null,
   constraint PK_PRICES primary key (ID_CONTAINERS, ID_CANDIES_TYPES)
);

/*==============================================================*/
/* Table : TEXTURES                                             */
/*==============================================================*/
create table candy_admin.TEXTURES
(
   ID_TEXTURES           INTEGER              not null,
   NAME                 CHAR(255)            not null,
   constraint PK_TEXTURES primary key (ID_TEXTURES)
);

/*==============================================================*/
/* Table : TRANSPORTS                                           */
/*==============================================================*/
create table candy_admin.TRANSPORTS
(
   ID_TRANSPORTS         INTEGER              not null,
   NAME                  CHAR(255)            not null,
   NB_PALLETS            INTEGER              not null,
   constraint PK_TRANSPORTS primary key (ID_TRANSPORTS)
);

/*==============================================================*/
/* Table : VARIETIES                                             */
/*==============================================================*/
create table candy_admin.VARIETIES
(
   ID_VARIETIES           INTEGER              not null,
   NAME                 CHAR(255)            not null,
   constraint PK_VARIETIES primary key (ID_VARIETIES)
);

alter table candy_admin.CANDIES
   add constraint FK_CANDIES_ASSOCIATI_CANDIES_ foreign key (ID_CANDIES_TYPES)
      references candy_admin.CANDIES_TYPES (ID_CANDIES_TYPES) ON DELETE CASCADE;

alter table candy_admin.CANDIES
   add constraint FK_CANDIES_ASSOCIATI_VARIETIES foreign key (ID_VARIETIES)
      references candy_admin.VARIETIES (ID_VARIETIES) ON DELETE CASCADE;

alter table candy_admin.CANDIES
   add constraint FK_CANDIES_ASSOCIATI_TEXTURES foreign key (ID_TEXTURES)
      references candy_admin.TEXTURES (ID_TEXTURES) ON DELETE CASCADE;

alter table candy_admin.CANDIES
   add constraint FK_CANDIES_ASSOCIATI_COLORS foreign key (ID_COLORS)
      references candy_admin.COLORS (ID_COLORS) ON DELETE CASCADE;

alter table candy_admin.CANDIES_ORDERS
   add constraint FK_CANDIES__CANDIES_O_ORDERS foreign key (ID_ORDERS)
      references candy_admin.ORDERS (ID_ORDERS) ON DELETE CASCADE;

alter table candy_admin.CANDIES_ORDERS
   add constraint FK_CANDIES__CANDIES_O_CANDIES foreign key (ID_CANDIES)
      references candy_admin.CANDIES (ID_CANDIES) ON DELETE CASCADE;

alter table candy_admin.COMPOSITIONS
   add constraint FK_COMPOSIT_COMPOSITI_CANDIES_ foreign key (ID_CANDIES_TYPES)
      references candy_admin.CANDIES_TYPES (ID_CANDIES_TYPES) ON DELETE CASCADE;

alter table candy_admin.COMPOSITIONS
   add constraint FK_COMPOSIT_COMPOSITI_COMPONEN foreign key (ID_COMPONENTS)
      references candy_admin.COMPONENTS (ID_COMPONENTS) ON DELETE CASCADE;

alter table candy_admin.CONDITIONNINGS
   add constraint FK_CONDITIO_ASSOCIATI_CONTAINE foreign key (ID_CONTAINERS)
      references candy_admin.CONTAINERS (ID_CONTAINERS) ON DELETE CASCADE;

alter table candy_admin.CONTAINER_CAPACITIES
   add constraint FK_CONTAINE_CONTAINER_TRANSPOR foreign key (ID_TRANSPORTS)
      references candy_admin.TRANSPORTS (ID_TRANSPORTS) ON DELETE CASCADE;

alter table candy_admin.CONTAINER_CAPACITIES
   add constraint FK_CONTAINER_CONTAINE2 foreign key (ID_CONTAINERS)
      references candy_admin.CONTAINERS (ID_CONTAINERS) ON DELETE CASCADE;

alter table candy_admin.CONTAINER_CAPACITIES
   add constraint FK_CONTAINER_CONTAINE1 foreign key (CON_ID_CONTAINERS)
      references candy_admin.CONTAINERS (ID_CONTAINERS) ON DELETE CASCADE;

alter table candy_admin.COUNTRIES
   add constraint FK_COUNTRIE_ASSOCIATI_TRANSPOR foreign key (ID_TRANSPORTS)
      references candy_admin.TRANSPORTS (ID_TRANSPORTS) ON DELETE CASCADE;

alter table candy_admin.MANUFACTURINGS
   add constraint FK_MANUFACT_ASSOCIATI_VARIETIES foreign key (ID_VARIETIES)
      references candy_admin.VARIETIES (ID_VARIETIES) ON DELETE CASCADE;

alter table candy_admin.ORDERS
   add constraint FK_ORDERS_ASSOCIATI_CLIENTS foreign key (ID_CLIENTS)
      references candy_admin.CLIENTS (ID_CLIENTS) ON DELETE CASCADE;

alter table candy_admin.ORDERS
   add constraint FK_ORDERS_ASSOCIATI_COUNTRIE foreign key (ID_COUNTRIES)
      references candy_admin.COUNTRIES (ID_COUNTRIES) ON DELETE CASCADE;

alter table candy_admin.PRICES
   add constraint FK_PRICES_PRICES_CANDIES_ foreign key (ID_CANDIES_TYPES)
      references candy_admin.CANDIES_TYPES (ID_CANDIES_TYPES) ON DELETE CASCADE;

alter table candy_admin.PRICES
   add constraint FK_PRICES_PRICES2_CONTAINE foreign key (ID_CONTAINERS)
      references candy_admin.CONTAINERS (ID_CONTAINERS) ON DELETE CASCADE;


